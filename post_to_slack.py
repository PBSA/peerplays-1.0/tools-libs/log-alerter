import requests
import json
import os
from dotenv import load_dotenv
load_dotenv()


def post_message_to_slack(slack_data):
    URL = os.environ.get('SLACK_URL')
    data = json.dumps(slack_data)
    response = requests.post(
        URL, json={"text": data},
        headers={'Content-Type': 'application/json'}
    )