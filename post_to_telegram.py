import requests
import os
from dotenv import load_dotenv
load_dotenv()


def post_message_to_telegram(tg_data):
    BOT_TOKEN = os.environ.get('TG_BOT_TOKEN')
    BOT_CHATID = URL = os.environ.get('TG_CHATID')
    data = 'https://api.telegram.org/bot' + BOT_TOKEN + '/sendMessage?chat_id=' + BOT_CHATID + '&parse_mode=Markdown&text=' + tg_data
    response = requests.get(data)
    return response.json()