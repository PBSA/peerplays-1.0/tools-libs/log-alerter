import time
import socket
import os
from post_to_slack import post_message_to_slack
from post_to_telegram import post_message_to_telegram
from dotenv import load_dotenv
load_dotenv()

hostname = socket.gethostname()
log_file = os.environ.get('LOG_FILE')
keywords_file = os.environ.get('KEYWORDS_FILE')

# This function reads a file line by line and waits for a new line once it reaches the EOF
def watch(file, words):
    # Print All - determines if matched words should print for existing lines in a file
    print_all = int(os.environ.get('PRINT_ALL'))
    # Print Delay - determines how long to wait after a match is found
    print_delay = float(os.environ.get('PRINT_DELAY'))
    with open(file, 'r') as f:
        while True:
            nl = f.readline()
            fnl = nl.lower().strip()
            if(fnl):
                if(print_all == 1):
                    for word in words:
                        if word.lower().rstrip('\n') in fnl:
                            yield (word, nl)
                            time.sleep(print_delay)
            else:
                print_all = 1
                # Avoid busy waiting
                time.sleep(1)

with open(keywords_file) as kf:
    keywords = kf.readlines()

for hit_word, hit_line in watch(log_file, keywords):
    print("Host: %r Found %r in line: %r" % (hostname, hit_word.rstrip('\n'), hit_line.rstrip('\n')))
    #post_message_to_slack("Host: %r Found %r in line: %r" % (hostname, hit_word.rstrip('\n'), hit_line.rstrip('\n')))
    #post_message_to_telegram("Host: %r Found %r in line: %r" % (hostname, hit_word.rstrip('\n'), hit_line.rstrip('\n')))
